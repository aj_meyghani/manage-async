const fs = require("fs-extra");

const files = ["files/file1.txt", "files/file2.txt"];
const output = "output";

async function prepare() {
  await fs.remove(output);
  return await fs.mkdir(output);
}

async function main() {
  await prepare();

  const readWrites = files.map(async file => {
    const content = await fs.readFile(file, "utf-8");
    const path = file.replace("files", output);
    return await fs.writeFile(path, content);
  });

  return await Promise.all(readWrites);
}

main()
.then(console.log)
.catch(err => console.log("An error occurred", err));
