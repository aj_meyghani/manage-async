const fs = require("fs-extra");

async function main() {
  const files = ["files/file1.txt", "files/file2.txt"];

  const readWrites = files.map(async file => {
    const content = await fs.readFile(file, "utf-8");
    return await fs.writeFile(file.replace(".txt", "-copy.txt"), content);
  });

  return await Promise.all(readWrites);
}

main()
.then(console.log)
.catch(err => console.log("An error occurred", err));
