const fs = require("fs-extra");

async function main() {
  const files = ["files/file1.txt", "files/file2.txt"];
  for (const file of files) {
    const content = await fs.readFile(file, "utf-8");
    const write = await fs.writeFile(file.replace(".txt", "-copy.txt"), content);
  }
  return files;
}

main()
.then(console.log)
.catch(err => console.log("An error occurred", err));
