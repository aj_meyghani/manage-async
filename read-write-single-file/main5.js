const fs = require("fs-extra");
const {error, call} = require("../call");

async function main() {
  const fileContent = await call(fs.readFile("./file.txt", "utf-8"));
  if(fileContent.error) {
    return error(fileContent, "Error while reading the file");
  }

  const writeResult = await call(fs.writeFile("./file-copy.txt", fileContent));
  if(writeResult.error) {
    return error(writeResult, "Error while writing the file");
  }

  return "file-copy.txt";
}

main()
.then(r => {
  if(r.error) {
    return console.log("An error occurred, recover here. Details:", r);
  }
  return console.log("Done, no error. Result:", r);
})
.catch(err => console.log("An error occurred", err));
