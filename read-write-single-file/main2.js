const util = require("util");
const fs = require("fs");
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

async function main() {
  let fileContent;
  try {
    fileContent = await readFile("./files.txt", "utf-8");
  } catch(err) {
    return {message: "Error while reading the file", error: err};
  }

  try {
    const writeResult = await writeFile("./file-copy.txt", fileContent);
  } catch(err) {
    return {message: "Error while writing the file", error: err};
  }

  return "file-copy5.txt";
}

main()
.then(r => {
  if(r.error) {
    return console.log("An error occurred, recover here. Details:", r);
  }
  return console.log("Done, no error. Result:", r);
})
.catch(err => console.log("An error occurred", err));
