const util = require("util");
const fs = require("fs");
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

async function main() {
  const fileContent = await readFile("./file.txt", "utf-8")
  .catch(err => ({
    message: "Error while reading the file", error: err,
  }));

  if (fileContent.error) {
    return fileContent;
  }

  const writeResult = await writeFile("./file-copy.txt", fileContent)
  .then(result => ({}))
  .catch(err => ({
    message: "Error while writing the file", error: err,
  }));

  if(writeResult.error) {
    return writeResult;
  }

  return "file-copy.txt";
}

main()
.then(r => {
  if(r.error) {
    return console.log("An error occurred, recover here. Details:", r);
  }
  return console.log("Done, no error. Result:", r);
})
.catch(err => console.log("An error occurred", err));
