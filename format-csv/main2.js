const fs = require("fs-extra");
const util = require("util");
const path = require("path");
const csvParse = util.promisify(require("csv-parse"));
const csvStringify = util.promisify(require("csv-stringify"));
const pLimit = require('p-limit');

async function setup() {
  const output = "output";
  await fs.remove(output);
  await fs.mkdir(output);
  return output;
}

async function csvFiles(inputFolder) {
  const dirContent = await fs.readdir(inputFolder);
  const paths = dirContent.map(c => path.join(inputFolder, c));

  return await Promise.all(paths.map(async p => {
    const isFileAndCSV = ((await fs.stat(p)).isFile() && /\.csv$/.test(p));
    return isFileAndCSV ? p : "";
  }))
  .then(paths => paths.filter(v => v));
}

function format(content) {
  return content.map((v, i) => {
    if(i === 0) {
      return v.map(h => h.toUpperCase());
    }
    return v;
  });
}

async function formatWrite(file, output) {
  const content = await fs.readFile(file, "utf-8");
  const parsed = await csvParse(content);
  const formatted = format(parsed);
  const stringified = await csvStringify(formatted);
  const outPath = path.join(output, file.split("/").slice(-1)[0]);
  await fs.writeFile(outPath, stringified);
  return file;
}

async function main() {
  const src = "input-folder";
  const [output, files] = await Promise.all([
    setup(), csvFiles(src),
  ]);

  /* limit concurrent tasks to 2 */
  const limit = pLimit(2);
  return await Promise.all(
    files.map(file => limit(() => formatWrite(file, output)))
  );
}

main()
.then(console.log)
.catch(console.log);
