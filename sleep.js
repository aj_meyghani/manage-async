const sleep = (t) => new Promise(r => setTimeout(() => {r(t)}, t));

module.exports = sleep;
